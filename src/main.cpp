#define DOCTEST_CONFIG_IMPLEMENT
#include "doctest.h"

#include <iostream>
#include <fstream>
#include <string>

#include "init.h"
#include "problem_parameters.h"
#include "darwin.h"

int main(int argc, char *argv[])
{
  ////////////////////
  // INITIALISATION //
  ////////////////////
  if(argc != 7)
  {
    std::cerr << "Invalid number of input parameters.\n";
    return -1;
  }

  Parameters::evolution_time = 1000;

  Parameters::flock_population = 1000;
  // Parameters::number_of_turns = 50;

  // Parameters::map_dim_x = 0;
  // Parameters::map_dim_y = 0;
  Parameters::start = Vec2(std::stod(argv[3]), std::stod(argv[4]));
  Parameters::end = Vec2(std::stod(argv[5]), std::stod(argv[6]));

  // Initialises the land map for trajectory calculations.
  init_land_map(argv[2]);
  // Initialises the first trajectory calculated with Dijkstra.
  init_initial_traj(argv[1]);
  // Initialises the statistics table of the ship needed for speed calculations.
  init_ship_stats();

  /////////////
  // DOCTEST //
  /////////////
  doctest::Context context;
  context.applyCommandLine(argc, argv);

  // Runs tests
  int res = context.run();

  // important - query flags (and --exit) rely on the user doing this
  if(context.shouldExit())
   return res;          // propagate the result of the tests

  /////////////
  // RUNTIME //
  /////////////
  // Runs the evolution and writes the best path found to the output stream.
  std::ofstream path_write("./out/path_plot.out");
  if(!path_write.is_open())
  {
    std::cerr << "Error opening \"./out/fitness_plot.out\"" << std::endl;
    return -1;
  }
  darwin(Parameters::evolution_time).write_to(path_write);
  path_write.close();

  return 0;
}
