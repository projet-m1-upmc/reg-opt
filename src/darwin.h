// Defines a function which runs the evolution process, creating and evolving a
// population of paths. The return value is the best path found.

#ifndef DARWIN_H
#define DARWIN_H

#include "Path_Flock.h"

Path darwin(const int evolution_time);

#endif // end DARWIN_H
