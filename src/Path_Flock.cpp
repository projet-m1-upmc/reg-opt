#include "Path_Flock.h"

#include <iostream>
#include <vector>
#include <list>
#include <random>
#include <algorithm>
#include <cmath>

#include "evaluate_path.h"
#include "Vec2.h"
#include "problem_parameters.h"

////////////////
// PATH CLASS //
////////////////

/// Static Member Initialization ///

// NOTE: RNG parameters set here.
std::random_device Path::random_device_{};
std::default_random_engine Path::rng_engine_(random_device_());
std::bernoulli_distribution Path::mutation_probability_(0.01);
// NOTE: Get better parameters.
std::normal_distribution<double> Path::mutation_spread_(0, 50);

/// Constructors ///

Path::Path(const std::list<Vec2>::size_type number_of_turns)
{
  path_turns_ = std::list<Vec2>(number_of_turns,
                                Vec2(0., 0.));

  int i=0;
  for(auto &turn : path_turns_)
  {
    double px = Parameters::init_traj[i*2];
    double py = Parameters::init_traj[i*2+1];
    turn = Vec2(px, py);
    i++;
  }

  this->update_fitness();
}

/// Member Functions ///

// Randomly mutates the position of turns, then does bound checking to make sure
// the trajectory hasn't left the map.
void Path::mutate()
{
  // Iterates through all the turns in the path for mutation.
  for(auto turn_ptr = path_turns_.begin();
      turn_ptr != path_turns_.end();
      ++turn_ptr)
  {
    if(mutation_probability_(rng_engine_))
    {
      (*turn_ptr).setX((*turn_ptr).getX()
                       + mutation_spread_(rng_engine_));
      (*turn_ptr).setY((*turn_ptr).getY()
                       + mutation_spread_(rng_engine_));
    }

    // Makes sure the points don't leave the map.
    while(turn_ptr->getX() < 0 || turn_ptr->getX() > Parameters::map_dim_x)
    {
      if(turn_ptr->getX() < 0)
      {
        turn_ptr->setX(-turn_ptr->getX());
      }

      if(turn_ptr->getX() > Parameters::map_dim_x)
      {
        turn_ptr->setX(Parameters::map_dim_x - turn_ptr->getX());
      }
    }

    while(turn_ptr->getY() < 0 || turn_ptr->getY() > Parameters::map_dim_y)
    {
      if(turn_ptr->getY() < 0)
      {
        turn_ptr->setY(-turn_ptr->getY());
      }

      if(turn_ptr->getY() > Parameters::map_dim_y)
      {
        turn_ptr->setY(Parameters::map_dim_y - turn_ptr->getY());
      }
    }
  }
}

// Calculates and sets the fitness for the current path.
void Path::update_fitness()
{
  double total_travel_time = {0.};
  Vec2 previous_turn = Parameters::start;

  // Calculates the travel time for each segment, then adds it to the total.
  for(auto path_iter = path_turns_.cbegin() ; path_iter != path_turns_.cend() ;
      ++path_iter)
  {
    if(sub(*path_iter, previous_turn).length() == 0)
    {
      continue;
    }

    total_travel_time += evaluate_segment(previous_turn, *path_iter);

    previous_turn = *path_iter;
  }

  total_travel_time += evaluate_segment(previous_turn, Parameters::end);

  fitness_ = total_travel_time;
}

// Writes out all the points of the trajectory to the ostream parameter.
void Path::write_to(std::ostream &write_stream)
{
  write_stream << Parameters::start.getX() << "\t" << Parameters::start.getY() << "\n";
  for(auto turn : path_turns_)
  {
    write_stream << turn.getX() << "\t" << turn.getY() << "\n";
  }
  write_stream << Parameters::end.getX() << "\t" << Parameters::end.getY() << "\n";
}

//////////////////////
// PATH_FLOCK CLASS //
//////////////////////

/// Static member initialisation ///

std::random_device Path_Flock::random_device_{};
std::default_random_engine Path_Flock::rng_engine_(random_device_());
std::bernoulli_distribution Path_Flock::cross_probability_(0.01);

/// Constructors ///

// Initializes the path flock then selects the best one as the base of mutation.
Path_Flock::Path_Flock(const int flock_population)
  : paths_(std::vector<Path>(flock_population,
                             Path(Parameters::number_of_turns))),
    best_path_(Path(Parameters::number_of_turns))
{
  // Sorts the Paths in order of decreasing travel time.
  std::sort(paths_.begin(), paths_.end(),
            [](Path path1, Path path2)
            { return path1.get_fitness() < path2.get_fitness(); });

  if(best_path_.get_fitness() < paths_.at(0).get_fitness())
  {
    best_path_ = paths_.at(0);
  }
}

/// Member functions ///

// TODO: Check the logic on this.
Path Path_Flock::cross_over()
{
  // Selection of 2 paths to cross-over, taking care not to overflow the vector.
  Path best_path_1{paths_.at(0)},
    best_path_2{paths_.at(1)};

  std::list<Vec2>::iterator path_ptr_2 = best_path_2.path_turns_.begin(),
    path_ptr_temp_1,
    path_ptr_temp_2;
  for(auto path_ptr_1 = best_path_1.path_turns_.begin() ;
      path_ptr_1 != best_path_1.path_turns_.end() ;
      ++path_ptr_1)
  {
    if(cross_probability_(rng_engine_))
    {
      path_ptr_temp_1 = ++path_ptr_1;
      path_ptr_temp_2 = ++path_ptr_2;
      std::swap_ranges(path_ptr_temp_1, best_path_1.path_turns_.end(), path_ptr_temp_2);
    }

    ++path_ptr_2;
  }

  return best_path_1;
}

void Path_Flock::evolve()
{
  for(auto path_ptr = paths_.rbegin() ; path_ptr != paths_.rend()-- ; ++path_ptr)
  {
    *path_ptr = this->cross_over();
    path_ptr->mutate();
    path_ptr->update_fitness();
  }

  // Sorts the Paths in order of decreasing travel time.
  std::sort(paths_.begin(), paths_.end(),
            [](Path path1, Path path2)
              { return path1.get_fitness() < path2.get_fitness(); });

  // Updates the best path.
  if(paths_.at(0).get_fitness() < best_path_.get_fitness())
  {
    best_path_ = paths_.at(0);
  }

  best_fitnesses_.push_back(best_path_.get_fitness());

  while(best_fitnesses_.size() > 100)
  {
    best_fitnesses_.pop_front();
  }
}

void Path_Flock::update_mutation_deviation(int epoch)
{
  std::cout << "Updated mutation deviation: "
            << (1. - (double)epoch/Parameters::evolution_time) * 10. << "\n";

  for(auto &path : paths_)
  {
    path.mutation_spread_
      = std::normal_distribution<double>(0,
                                         (1. - (double)epoch/Parameters::evolution_time)
                                               * 10.);
  }
}
