#include "evaluate_path.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>

#include "problem_parameters.h"


Vec2 wind_eval(int mapX, int mapY)
{
  if (mapX < 1500 && mapY > 0) {
    return Vec2(10., 0.);
  } else {
    return Vec2(0.1, 10.);
  }
}

double speed(const Vec2& wind, const Vec2& ship_direction)
{
  // The parameter angle for the wind sets 0 degrees as wind coming from behind
  // the boat, and the value returned by angle() is in radians so needs to be
  // converted.
  double angle = std::abs(std::abs((ship_direction.angle() - wind.angle())
                                   * 180.0/M_PI) - 180.0);

  // The file starts at 30 degrees.
  std::vector<std::vector<int>>::size_type angle_index = (int)(angle - 30)/5;
  std::vector<int>::size_type wind_speed = (int)wind.length();
  if (angle_index < 31)
  {
    if (wind_speed < Parameters::ship_stats.at(angle_index).size())
    {
      return Parameters::ship_stats.at(angle_index).at(wind_speed);
    }
    else
    {
      return Parameters::ship_stats.at(angle_index).back();
    }
  }
  else
  {
    return 0.2;
  }
}

// Returns the distance travelled over land if necessary.
double land_travelled(int mapX, int mapY, double land_travelled)
{
  // land : 0, sea : 1
  if(Parameters::land_map.at(mapY).at(mapX) == 0)
  {
    return land_travelled;
  }
  return 0.;
}

// Calculates a quadratic penalty based on the total land crossed in a segment.
// NOTE: Quadratic coefficients set here.
double land_penalty(double total_land_travelled)
{
  return 100 * std::pow(total_land_travelled, 2);
}

// Uses a digital differential analysis technique inspired from raycasting, used
// to determine the distance to a wall in old 2D games.
double evaluate_segment(Vec2 start, Vec2 end)
{
  double total_fitness = {0.};
  double total_travel_duration = {0.};

  // Running total of land travelled during a segment.
  double total_land_travelled = {0.};

  // Direction of travel
  Vec2 direction = sub(end, start).normalize();

  // Which box of the map we're in
  int mapX = (int)(start.getX());
  int mapY = (int)(start.getY());
  int endX = (int)(end.getX());
  int endY = (int)(end.getY());

  // Checks that both points are not in the same square.
  if(mapX == endX && mapY == endY)
  {
    total_fitness += sub(end, start).length() / speed(wind_eval(mapX, mapY), direction);

    total_land_travelled += land_travelled(mapX, mapY, sub(end, start).length());
    total_fitness += land_penalty(total_land_travelled);

    return total_fitness;
  }

  // Projections of direction onto X and Y axes
  double ray_dir_X = direction.getX();
  double ray_dir_Y = direction.getY();

  // Length of ray from current position to next x or y-side
  double side_dist_X = 0;
  double side_dist_Y = 0;

  // Holds the distance travelled in the current square.
  double dist_in_square;

  // Length of ray from one x or y-side to next x or y-side
  // TODO: Check for division by zero.
  double delta_dist_X = sqrt(1 + std::pow(ray_dir_Y/ray_dir_X, 2));
  double delta_dist_Y = sqrt(1 + std::pow(ray_dir_X/ray_dir_Y, 2));
  double delta_dist_min = std::min({delta_dist_X, delta_dist_Y});
  double delta_dist_max = std::max({delta_dist_X, delta_dist_Y});

  // What direction to step in x or y-direction (either +1 or -1)
  int stepX;
  int stepY;

  // Determines the direction of travel along x and y-axes.
  if (ray_dir_X < 0)
  {
    stepX = -1;
    side_dist_X = (start.getX() - mapX) * delta_dist_X;
  }
  else
  {
    stepX = 1;
    side_dist_X = (mapX + 1.0 - start.getX()) * delta_dist_X;
  }
  if (ray_dir_Y < 0)
  {
    stepY = -1;
    side_dist_Y = (start.getY() - mapY) * delta_dist_Y;
  }
  else
  {
    stepY = 1;
    side_dist_Y = (mapY + 1.0 - start.getY()) * delta_dist_Y;
  }

  // Calculates the distance travelled in the first square, and increments
  // total_land_travelled if, necessary.
  dist_in_square = std::min({side_dist_X, side_dist_Y});
  total_travel_duration += dist_in_square
                           / speed(wind_eval(mapX, mapY), direction);
  total_land_travelled += land_travelled(mapX, mapY, dist_in_square);

  // Performs the calculations on the whole of the segment, minus the first and
  // last squares (which contain the start and end of the segment).
  // TODO: Fix bug where going straight down counts 1 more square travelled on
  //       land than going straight up (test case)
  bool switched = false;
  while (mapX != endX || mapY != endY)
  {
    // Jump to next map square, OR in x-direction, OR in y-direction
    if (mapX != endX && side_dist_X < side_dist_Y)
    {
      side_dist_X += delta_dist_X;
      mapX += stepX;

      if(delta_dist_X > delta_dist_Y)
        switched = true;
      else switched = false;
    }
    else if(mapY != endY)
    {
      side_dist_Y += delta_dist_Y;
      mapY += stepY;

      if(delta_dist_X < delta_dist_Y)
        switched = true;
      else switched = false;
    }
    else break;

    if(switched)
    {
      dist_in_square = delta_dist_min
        - std::abs(std::abs(side_dist_X - side_dist_Y) - delta_dist_max);
    }
    else
    {
      dist_in_square = std::min(delta_dist_min, std::abs(side_dist_X - side_dist_Y));
    }
    total_travel_duration += dist_in_square / speed(wind_eval(mapX, mapY), direction);
    total_land_travelled += land_travelled(mapX, mapY, dist_in_square);
  }

  // Deals with the last portion of the segment in the final square (up to
  // the end point).
  if (ray_dir_X > 0)
  {
    side_dist_X = (end.getX() - mapX) * delta_dist_X;
  }
  else
  {
    side_dist_X = (mapX + 1.0 - end.getX()) * delta_dist_X;
  }
  if (ray_dir_Y > 0)
  {
    side_dist_Y = (end.getY() - mapY) * delta_dist_Y;
  }
  else
  {
    side_dist_Y = (mapY + 1.0 - end.getY()) * delta_dist_Y;
  }
  dist_in_square = std::min({side_dist_X, side_dist_Y});
  total_travel_duration += dist_in_square
                           / speed(wind_eval(mapX, mapY), direction);
  total_land_travelled += land_travelled(mapX, mapY, dist_in_square);

  // Applies the quadratic penalty for travelling over land.
  total_fitness = total_travel_duration + land_penalty(total_land_travelled);

  return total_fitness;
}
