#include "init.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "problem_parameters.h"

void init_land_map(std::string land_map_file)
{
  std::ifstream read(land_map_file);
  if (!read.is_open())
  {
    std::cerr << "There was a problem opening the landform map input file!\n";
    exit(1); // Exit or do additional error checking
  }

  Parameters::land_map.clear();

  int temp_map_square;
  std::string temp_map_line_str;
  std::deque<int> temp_map_line;
  while(getline(read, temp_map_line_str))
  {
    std::istringstream line_ss(temp_map_line_str);
    temp_map_line.clear();
    while(line_ss >> temp_map_square)
    {
      if(temp_map_square != 0 && temp_map_square != 1)
      {
        std::cerr << "Invalid landform map input file.\n" << std::endl;
        exit(1); // Exit or do additional error checking
      }

      temp_map_line.push_back(temp_map_square);
    }

    // (x, y)-coordinates have their origin at the bottom left but the file is
    // read from the top, so it is inverted using push_front.
    Parameters::land_map.push_front(temp_map_line);

    read.clear();
  }

  Parameters::map_dim_x = Parameters::land_map.at(0).size();
  Parameters::map_dim_y = Parameters::land_map.size();
}

void init_initial_traj(std::string initial_traj_file)
{
  std::ifstream read(initial_traj_file);
  if (!read.is_open()) {
    std::cerr << "There was a problem opening the ship stats input file!\n";
    exit(1); // Exit or do additional error checking
  }
  read >> Parameters::number_of_turns;
  for (int i = 0; i < Parameters::number_of_turns; i++) {
    for (int j = 0; j < 2; j++) {
      read >> Parameters::init_traj[i*2 + j];
    }
  }
}

void init_ship_stats()
{
  std::ifstream read("./res/ship_stats.txt");
  if (!read.is_open()) {
    std::cerr << "There was a problem opening the ship stats input file!\n";
    exit(1); // Exit or do additional error checking
  }

  Parameters::ship_stats.clear();

  double temp_ship_stat;
  std::string stats_line_str;
  std::vector<double> temp_stats_line;
  while(getline(read, stats_line_str))
  {
    std::istringstream stats_line_ss(stats_line_str);
    temp_stats_line.clear();

    while(stats_line_ss >> temp_ship_stat)
    {
      if(temp_ship_stat < 0)
      {
        std::cerr << "Invalid ship stats input file.\n";
        exit(1); // Exit or do additional error checking
      }

      temp_stats_line.push_back(temp_ship_stat);
    }

    Parameters::ship_stats.push_back(temp_stats_line);
  }

  if(Parameters::ship_stats.empty())
  {
    std::cerr << "Empty ship stats input file.";
    exit(1);
  }
}
