#include "darwin.h"

#include <iostream>
#include <fstream>

#include "problem_parameters.h"

Path darwin(const int evolution_time)
{
  // NOTE: paths initialisation here
  Path_Flock paths(Parameters::flock_population);

  std::ofstream fitness_write("./out/fitness_plot.out");
  if(!fitness_write.is_open())
  {
    std::cerr << "Error opening \"./out/fitness_plot.out\"" << std::endl;
    // Could do better error handling.
    exit(1);
  }

  std::cout << "Starting evolution..." << "\n";
  for(int epoch = 0 ; epoch < evolution_time ; ++epoch)
  {
    paths.evolve();

    // Writes progress during runtime.
    if(epoch % 5 == 0)
    {
      std::cout << "Epoch: " << epoch
                << " (" << epoch*100/evolution_time << "%)" << "\n" ;
      std::cout << paths.get_best_path().get_fitness() << "\n";
    }
    if(epoch % 100 == 0)
    {
      std::ofstream path_write("./out/path_plot.out");
      if(!fitness_write.is_open())
      {
        std::cerr << "Error opening \"./out/path_plot.out\"" << std::endl;
        // Could do better error handling.
        exit(1);
      }
      paths.get_best_path().write_to(path_write);
    }

    // Decreases the deviation of mutations over the course of evolution.
    if(epoch % 200 == 199)
    {
      paths.update_mutation_deviation(epoch);
    }

    // Writes a graph of the fitness of the best path as a function of epoch.
    fitness_write << epoch << "\t"
                  << paths.get_best_path().get_fitness() << "\n";
  }
  fitness_write << "Finished.\n";
  fitness_write.close();

  return paths.get_best_path();
}
