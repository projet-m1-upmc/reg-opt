#include "problem_parameters.h"

#include <vector>
#include <deque>

#include "Vec2.h"

namespace Parameters
{
  int evolution_time,
    flock_population,
    number_of_turns;

  int map_dim_x,
    map_dim_y;

  Vec2 start(0., 0.),
    end(1000, 1000);


  double init_traj[1000*2];

  std::vector<std::vector<double>> ship_stats;
  std::deque<std::deque<int>> land_map;
}
