#ifndef VECH_H
#define VECH_H

class Vec2
{
public:
  // CONSTRUCTORS //
  explicit Vec2(double x, double y);

  // GETTERS //
  inline double getX() const { return this->x_; }
  inline double getY() const { return this->y_; }

  // SETTERS //
  inline void setX(double new_x) { this->x_ = new_x; }
  inline void setY(double new_y) { this->y_ = new_y; }

  // MEMBER FUNCTIONS //
  double length() const;
  double angle() const;
  Vec2 normalize();
  void rotate(double a);

  bool is_nul() const;

private:
  double x_,y_;
};

// Vec2 Friend Functions //
double dot(const Vec2 &, const Vec2 &);
Vec2 add(const Vec2 &, const Vec2 &);
Vec2 sub(const Vec2 &, const Vec2 &);
Vec2 mul(double, const Vec2 &);


#endif // end VECH_H
