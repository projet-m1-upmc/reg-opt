#ifndef PROBLEM_PARAMETERS_H
#define PROBLEM_PARAMETERS_H

#include <vector>
#include <deque>

#include "Vec2.h"

namespace Parameters
{
  extern int evolution_time,
    flock_population,
    number_of_turns;

  extern int map_dim_x,
    map_dim_y;

  extern Vec2 start,
    end;

  extern double init_traj[1000*2];
  extern std::vector<std::vector<double>> ship_stats;
  // X and Y coordinates are inversed: access is written as:
  //   land_map.at(Y).at(X)
  extern std::deque<std::deque<int>> land_map;
}

#endif // end PROBLEM_PARAMETERS_H
