// Defines 2 classes: one which holds a path from the start to the finish, and a
// a second to manage the current generation of paths in the evolution process.

// TODO: touch up mutation probability.

#ifndef PATH_FLOCK_H
#define PATH_FLOCK_H

#include <deque>
#include <vector>
#include <list>
#include <random>

#include "Vec2.h"

// Defines a class which holds the list of points in a path between the start
// and finish.
class Path
{
  friend class Path_Flock;

private:
  /// RNG Utilities ///
  static std::random_device random_device_;
  static std::default_random_engine rng_engine_;
  // Probability that the position of a turn mutates.
  static std::bernoulli_distribution mutation_probability_;
  // Distribution of the mutations in the position of turns.
  static std::normal_distribution<double> mutation_spread_;

  /// Member Variables ///
  std::list<Vec2> path_turns_;
  double fitness_;

public:
  /// Constructors ///
  explicit Path(const std::list<Vec2>::size_type number_of_turns);

  /// Getters ///
  double get_fitness() const { return fitness_; };

  /// Member functions ///
  void mutate();
  // Calculates and sets the fitness.
  void update_fitness();
  void write_to(std::ostream &);
};

// Defines the class which manages the evolutions and selection of classes.
class Path_Flock
{
public:
  using size_type = std::vector<Path>::size_type;

private:
  /// RNG utilities ///
  static std::random_device random_device_;
  static std::default_random_engine rng_engine_;
  static std::bernoulli_distribution cross_probability_;

  // Deviation of mutations
  std::deque<double> best_fitnesses_;
  double best_fitness_deviation_;

  // Holds a sorted vector of the paths currently undergoing evolution.
  std::vector<Path> paths_;
  Path best_path_;

public:
  /// Constructors ///
  explicit Path_Flock(const int flock_population);

  /// Getters ///
  Path get_best_path() { return best_path_; }

  /// Member Functions ///

  // Mutates all the paths in the flock based off of the current best, while
  // keeping best_path up to date.
  void evolve();

  // Scrambles the 2 lists of turns (but maintains relative order in both paths).
  Path cross_over();

  // Updates the spread of values for mutations, so that smaller mutations occur
  // at the end evolution.
  void update_mutation_deviation(int epoch);
};

#endif // end PATH_FLOCK_H
