// Functions used to calculate the time taken to travel a given segment.
// The central function is evaluate_segment() which does the calculations
// on a single segment.

#ifndef EVALUATE_PATH_H
#define EVALUATE_PATH_H

#include "Vec2.h"

// Returns the speed of the ship given a wind vector and ship direction vector.
double speed(const Vec2& wind, const Vec2& direction);

// Function to calculate the travel time on a segment of a path (passed as argument).
double evaluate_segment(Vec2 start, Vec2 end);

#endif // end EVALUATE_PATH_H
