#include "Vec2.h"

#include <cmath>
#include <memory>

// Constructors //
Vec2::Vec2(double x, double y)
{
  this->x_ = x;
  this->y_ = y;
}

// Vector Functions //
double dot(const Vec2 &a, const Vec2 &b)
{
  return a.getX()*b.getX() + a.getY()*b.getY();
}

Vec2 add(const Vec2 &a, const Vec2 &b)
{
  return Vec2(a.getX() + b.getX(), a.getY() + b.getY());
}

Vec2 sub(const Vec2 &a, const Vec2 &b)
{
  return Vec2(a.getX() - b.getX(), a.getY() - b.getY());
}

Vec2 mul(double k, const Vec2 &vector)
{
  double x = vector.getX() * k,
         y = vector.getY() * k;

  return Vec2(x, y);
}

// Member Functions //
double Vec2::length() const
{
  return sqrt(dot((*this), (*this)));
}

double Vec2::angle() const
{
  return atan2(this->y_, this->x_);
}

Vec2 Vec2::normalize()
{
  double l = this->length();
  if (l == 0) return *this;
  else {
    this->x_ = this->x_/l;
    this->y_ = this->y_/l;
  }
  return *this;
}

void Vec2::rotate(double a)
{
  double x = this->getX(),
         y = this->getY();
  this->x_ = cos(a)*x - sin(a)*y;
  this->y_ = sin(a)*x + cos(a)*y;
}

bool Vec2::is_nul() const
{
  if(this->length() == 0)
    return true;
  else
    return false;
}
