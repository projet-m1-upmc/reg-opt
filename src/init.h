// Functions to initialise the arrays holding the information on the maps for
// landmasses to avoid, the wind speed at different points, and the stats of
// the ship and sail.

// TODO: Implement wind maps.

#ifndef INIT_H
#define INIT_H

#include <string>

// Initialises the array holding the ship characteristics.
void init_ship_stats();

// Initialises the array holding the map of landmasses.
// NOTE: init_land_map() must be called before init_initial_traj(), as it sets the
//       Parameters namespace constants for the map dimensions.
void init_land_map(std::string land_map_file);
void init_initial_traj(std::string initial_traj_file);

#endif // end INIT_H
