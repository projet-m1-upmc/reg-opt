#include "doctest.h"

#include "evaluate_path.h"

#include <cmath>

#include "Vec2.h"
#include "problem_parameters.h"

TEST_CASE("Testing the DDA function")
{
  CHECK(evaluate_segment(Vec2(0., 0.), Vec2(0., 0.)) < 0.01);
  CHECK(std::abs(evaluate_segment(Vec2(200, 1), Vec2(200, 150))
                 - evaluate_segment(Vec2(200, 150), Vec2(200, 1))) < 1);
}
