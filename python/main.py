import os
import sys

from convert_map import convert_map
from dijkstra_trajectory import dijkstra_trajectory

if __name__ == "__main__":
  map_image_filename = raw_input("Enter path to map image: ")
  if(os.path.exists(map_image_filename)):

    start_X = raw_input("Enter start X-coordinate: ")
    start_Y = raw_input("Enter start Y-coordinate: ")
    end_X = raw_input("Enter end X-coordinate: ")
    end_Y = raw_input("Enter end Y-coordinate: ")

    print("Converting map to boolean table...")
    convert_map(map_image_filename)
    print(" Done.\nCalculating initial trajectory...")
    dijkstra_trajectory(map_image_filename,
                        int(start_X), int(start_Y),
                        int(end_X), int(end_Y))
    print(" Done.\n")
  else:
    sys.exit("Invalid path.\n")


  print("./bin/evolution temp/initial_traj.res temp/land_map.txt "
             + start_X + " " + start_Y + " " + end_X + " " + end_Y + "\n")
  os.system("./bin/evolution temp/initial_traj.res temp/land_map.txt "
             + start_X + " " + start_Y + " " + end_X + " " + end_Y)

  os.system("rm -rf ./temp/*")
