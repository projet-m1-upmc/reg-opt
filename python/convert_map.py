import matplotlib.pyplot as plt
from Heap import Heap
from PIL import Image

def access(terrain, x, y, w, h):
    if x<0 or y<0 or x>=w or y>=h or terrain[x,y] == (255,255,255):
        return False
    return True

def convert_map(in_file):
    im = Image.open(in_file)
    rgb_im = im.convert('RGB')
    pixels = rgb_im.load()
    width, height = im.size

    out_file = open("./temp/land_map.txt", "a")

    for y in range(0, height):
        for x in range(0, width):
            if access(pixels, x, y, width, height):
                out_file.write("1 ") 
            else:
                out_file.write("0 ")
        out_file.write("\n")

    out_file.close()
