import matplotlib.pyplot as plt
from Heap import Heap
from PIL import Image

directions = [[0,-1],[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1]]
weight = [10,14,10,14,10,14,10,14]

def access(terrain, x, y, w, h):
    if x<0 or y<0 or x>=w or y>=h or terrain[x,y] == (255,255,255):
        return False
    return True

def dijkstra(terrain, w, h, arrival):
    infini = w*h*10
    distances = [[infini for x in range(w)] for y in range(h)]
    Q = Heap()
    for i in range (0, len(arrival)):
        Q.push(0, arrival[i])
        p = arrival[i]
        distances[p[1]][p[0]] = 0
    while len(Q)>0:
        s = Q.pop()
        for d in range (0, len(directions)):
            x = s[0] + directions[d][0]
            y = s[1] + directions[d][1]
            if access(terrain, x, y, w, h) and distances[y][x] == infini:
                distances[y][x] = distances[s[1]][s[0]] + weight[d]
                Q.push(distances[y][x], [x,y])
        #print len(Q)
    return distances

def trajectory(terrain, w, h, distances, start):
    pos = start
    cur_dir = -1
    trajectory = []
    nb_squares = 0
    while distances[pos[1]][pos[0]] > 0:
        nb_squares += 1
        #terrain[pos[0],pos[1]] = (255,255,0)
        min_pos = pos
        cur_pos = [0, 0]
        min_i = 0
        for i in range(0, len(directions)):
            cur_pos[0] = pos[0] + directions[i][0]
            cur_pos[1] = pos[1] + directions[i][1]
            if distances[min_pos[1]][min_pos[0]] > distances[cur_pos[1]][cur_pos[0]]:
                min_pos = [cur_pos[0], cur_pos[1]]
                min_i = i
        pos = min_pos
        if cur_dir != min_i:
            cur_dir = min_i
        if nb_squares%20 == 0:
            trajectory.append(pos)
    return trajectory

def dijkstra_trajectory(in_file, startX, startY, endX, endY):
    im = Image.open(in_file)
    rgb_im = im.convert('RGB')
    pixels = rgb_im.load()
    width, height = im.size

    arrival = [[endX, height - endY]]
    distances = dijkstra(pixels, width, height, arrival)

    for x in range(0, width):
        for y in range (0, height):
            if pixels[x,y] != (255,255,255):
                pixels[x,y] = (distances[y][x]%255,0,0)
    start = [startX, height - startY]
    traj = trajectory(pixels, width, height, distances, start)
    traj_x = [p[0] for p in traj];
    traj_y = [p[1] for p in traj];

    out_file = open("./temp/initial_traj.res", "a")
    out_file.write(str(len(traj))+"\n")
    for p in traj:
        out_file.write(str(p[0])+" "+ str(height-p[1]) + "\n")
    out_file.close()

    # plt.imshow(rgb_im)
    # plt.plot(traj_x, traj_y)
    # plt.show()
