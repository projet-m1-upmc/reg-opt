CFLAGS =-std=c++11
TEST_FLAGS = -Wall -Wextra
DEBUG_FLAGS =-Wall -Wextra -ggdb
SRC_DIR =src/
BUILD_DIR =build/
BIN_DIR =bin/

SRC_FILES = $(wildcard $(SRC_DIR)*.cpp)
OBJ_FILES = $(addprefix $(BUILD_DIR), $(notdir $(SRC_FILES:.cpp=.o)))
TEST_OBJ_FILES = $(addprefix $(BUILD_DIR), $(notdir $(SRC_FILES:.cpp=_test.o)))
DEBUG_OBJ_FILES = $(addprefix $(BUILD_DIR), $(notdir $(SRC_FILES:.cpp=_debug.o)))

clean:
	rm -f $(BIN_DIR)* $(BUILD_DIR)*.o

$(BIN_DIR)evolution: $(SRC_FILES)
	g++ $(CFLAGS) -O2 -o $@ $(SRC_FILES)

$(BIN_DIR)debug: $(DEBUG_OBJ_FILES)
	g++ $(CFLAGS) $(DEBUG_FLAGS) -o $@ $(DEBUG_OBJ_FILES)

$(BUILD_DIR)%_debug.o: $(SRC_DIR)%.cpp
	g++ $(CFLAGS) $(DEBUG_FLAGS) -o $@ -c $<

$(BIN_DIR)test: $(TEST_OBJ_FILES)
	g++ $(CFLAGS) $(TEST_FLAGS) -o $@ $(TEST_OBJ_FILES)

$(BUILD_DIR)%_test.o: $(SRC_DIR)%.cpp
	g++ $(CFLAGS) $(TEST_FLAGS) -o $@ -c $<

debug:	$(BIN_DIR)debug

test: $(BIN_DIR)test
	./$(BIN_DIR)test temp/initial_traj.res temp/land_map.txt 150 610 1950 610

compile: $(BIN_DIR)evolution
